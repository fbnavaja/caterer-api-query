package com.fbnavaja.caterer.api.database.jpa.data;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.persistence.Id;
import java.time.LocalDateTime;

@Document
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Getter
public class CatererEventData {
    @Id
    private String id;
    private String catererId;
    private String catererName;
    private String eventDescription;
    private LocalDateTime createdDateTime;
}
