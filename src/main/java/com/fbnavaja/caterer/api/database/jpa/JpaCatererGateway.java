package com.fbnavaja.caterer.api.database.jpa;

import com.fbnavaja.caterer.api.database.jpa.data.CatererData;
import com.fbnavaja.caterer.api.database.jpa.repositories.CatererRepository;
import com.fbnavaja.caterer.api.domain.entities.Caterer;
import com.fbnavaja.caterer.api.ports.database.CatererGateway;

import java.util.Collection;
import java.util.Optional;
import java.util.stream.Collectors;

public class JpaCatererGateway implements CatererGateway {
    private final CatererRepository catererRepository;

    public JpaCatererGateway(final CatererRepository catererRepository) {

        this.catererRepository = catererRepository;
    }

    @Override
    public Collection<Caterer> getAllCaterers() {

        return catererRepository.findAll()
                .stream()
                .map(this::mapToCaterer)
                .collect(Collectors.toList());
    }

    @Override
    public Caterer getCatererById(String catererId) {
        return Optional.ofNullable(mapToCaterer(catererRepository.findById(catererId).get())).orElse(mapToCaterer(CatererData.builder().build()));
    }

    @Override
    public String createCaterer(Caterer caterer) {

        return add(caterer);
    }

    @Override
    public void deleteCaterer(String catererId) {
        catererRepository.deleteById(catererId);
    }

    @Override
    public void saveCaterer(Caterer caterer) {
        update(caterer);
    }

    @Override
    public Collection<Caterer> getCatererByName(String catererName) {
        return catererRepository.findByName(catererName)
                .stream()
                .map(this::mapToCaterer)
                .collect(Collectors.toList());

    }

    private Caterer mapToCaterer(CatererData catererData) {
        return Caterer.builder()
                .id(catererData.getId())
                .name(catererData.getName())
                .location(buildLocation(catererData))
                .capacity(buildCapacity(catererData))
                .contactInfo(buildContactInfo(catererData))
                .build();
    }


    private Caterer.ContactInfo buildContactInfo(CatererData request) {
        return Caterer.ContactInfo.builder().mobileNumber(request.getMobileNumber()).phoneNumber(request.getPhoneNumber()).emailAddress(request.getEmailAddress()).build();
    }

    private Caterer.Capacity buildCapacity(CatererData request) {
        return Caterer.Capacity.builder().min_guests(request.getMin_guests()).max_guests(request.getMax_guests()).build();
    }

    private Caterer.Location buildLocation(CatererData request) {
        return Caterer.Location.builder().city(request.getCity()).street(request.getStreet()).zip(request.getZip()).build();
    }

    private String add(Caterer caterer) {

        CatererData catererData = CatererData
                .builder()
                .name(caterer.getName())
                .city(caterer.getLocation().getCity())
                .street(caterer.getLocation().getStreet())
                .zip(caterer.getLocation().getZip())
                .min_guests(caterer.getCapacity().getMin_guests())
                .max_guests(caterer.getCapacity().getMax_guests())
                .mobileNumber(caterer.getContactInfo().getMobileNumber())
                .phoneNumber(caterer.getContactInfo().getPhoneNumber())
                .emailAddress(caterer.getContactInfo().getEmailAddress())
                .build();

        return catererRepository.save(catererData).getId();
    }

    private String update(Caterer caterer) {

        CatererData catererData = CatererData
                .builder()
                .id(caterer.getId())
                .name(caterer.getName())
                .city(caterer.getLocation().getCity())
                .street(caterer.getLocation().getStreet())
                .zip(caterer.getLocation().getZip())
                .min_guests(caterer.getCapacity().getMin_guests())
                .max_guests(caterer.getCapacity().getMax_guests())
                .mobileNumber(caterer.getContactInfo().getMobileNumber())
                .phoneNumber(caterer.getContactInfo().getPhoneNumber())
                .emailAddress(caterer.getContactInfo().getEmailAddress())
                .build();

        return catererRepository.save(catererData).getId();
    }

}
