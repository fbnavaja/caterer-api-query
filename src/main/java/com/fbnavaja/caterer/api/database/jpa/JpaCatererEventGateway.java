package com.fbnavaja.caterer.api.database.jpa;

import com.fbnavaja.caterer.api.database.jpa.data.CatererEventData;
import com.fbnavaja.caterer.api.database.jpa.repositories.CatererEventRepository;
import com.fbnavaja.caterer.api.domain.entities.CatererEvent;
import com.fbnavaja.caterer.api.ports.database.CatererEventGateway;

import java.util.Collection;
import java.util.Optional;
import java.util.stream.Collectors;

public class JpaCatererEventGateway implements CatererEventGateway {
    private final CatererEventRepository catererEventRepository;

    public JpaCatererEventGateway(CatererEventRepository catererEventRepository) {
        this.catererEventRepository = catererEventRepository;
    }


    @Override
    public Collection<CatererEvent> getAllEvents() {

        return catererEventRepository.findAll()
                .stream()
                .map(this::mapToCatererEvent)
                .collect(Collectors.toList());
    }

    @Override
    public CatererEvent getCatererEventById(String eventId) {
        return Optional.ofNullable(mapToCatererEvent(catererEventRepository.findById(eventId).get())).orElse(mapToCatererEvent(CatererEventData.builder().build()));
    }

    @Override
    public void createCatererEvent(CatererEvent catererEvent) {

        add(catererEvent);
    }

    private CatererEvent mapToCatererEvent(CatererEventData catererEventData) {
        return CatererEvent.builder()
                .id(catererEventData.getId())
                .catererId(catererEventData.getCatererId())
                .catererName(catererEventData.getCatererName())
                .eventDescription(catererEventData.getEventDescription())
                .createdDateTime(catererEventData.getCreatedDateTime())
                .build();
    }

    private String add(CatererEvent catererEvent) {

        CatererEventData catererEventData = CatererEventData
                .builder()
                .id(catererEvent.getId())
                .catererId(catererEvent.getCatererId())
                .catererName(catererEvent.getCatererName())
                .eventDescription(catererEvent.getEventDescription())
                .createdDateTime(catererEvent.getCreatedDateTime())
                .build();

        return catererEventRepository.save(catererEventData).getId();
    }

}
