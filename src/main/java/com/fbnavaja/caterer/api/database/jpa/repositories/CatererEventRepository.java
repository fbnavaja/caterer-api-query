package com.fbnavaja.caterer.api.database.jpa.repositories;

import com.fbnavaja.caterer.api.database.jpa.data.CatererData;
import com.fbnavaja.caterer.api.database.jpa.data.CatererEventData;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

public interface CatererEventRepository extends MongoRepository<CatererEventData, String> {
}
