package com.fbnavaja.caterer.api.database.jpa.repositories;

import com.fbnavaja.caterer.api.database.jpa.data.CatererData;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@RepositoryRestResource(collectionResourceRel = "caterers", path = "caterers")
public interface CatererRepository extends MongoRepository<CatererData, String> {
    List<CatererData> findByName(String name);
}
