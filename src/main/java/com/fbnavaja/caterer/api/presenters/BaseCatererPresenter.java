package com.fbnavaja.caterer.api.presenters;

import com.fbnavaja.caterer.api.ports.presenters.Caterer;
import com.fbnavaja.caterer.api.ports.usecases.CatererResponse;

import java.time.format.DateTimeFormatter;

public class BaseCatererPresenter {
    private static final DateTimeFormatter DATE_TIME_FORMATTER = DateTimeFormatter.ISO_DATE;

    protected BaseCatererPresenter() {}

    public static Caterer mapToCaterer(CatererResponse response) {
        return Caterer
                .builder()
                .id(response.getId())
                .name(response.getName())
                .location(getCatererLocation(response))
                .capacity(getCatererCapacity(response))
                .contactInfo(getCatererContactInfo(response))
                .build();
    }

    private static Caterer.Location getCatererLocation(CatererResponse response) {
        return Caterer.Location
                .builder()
                .city(response.getCity())
                .street(response.getStreet())
                .zip(response.getZip())
                .build();
    }

    private static Caterer.Capacity getCatererCapacity(CatererResponse response) {
        return Caterer.Capacity
                .builder()
                .min_guests(response.getMin_guests())
                .max_guests(response.getMax_guests())
                .build();
    }

    private static Caterer.ContactInfo getCatererContactInfo(CatererResponse response) {
        return Caterer.ContactInfo
                .builder()
                .phoneNumber(response.getPhoneNumber())
                .mobileNumber(response.getMobileNumber())
                .emailAddress(response.getEmailAddress())
                .build();
    }
}
