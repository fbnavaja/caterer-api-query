package com.fbnavaja.caterer.api.presenters;

import com.fbnavaja.caterer.api.ports.presenters.Caterer;
import com.fbnavaja.caterer.api.ports.presenters.CatererOutput;
import com.fbnavaja.caterer.api.ports.presenters.Caterers;
import com.fbnavaja.caterer.api.ports.presenters.CaterersOutput;
import com.fbnavaja.caterer.api.ports.usecases.CatererResponse;
import com.fbnavaja.caterer.api.ports.usecases.get.caterers.CaterersResponse;

public class CaterersPresenter extends  BaseCatererPresenter implements CaterersOutput {
    private Caterers caterers;

    public Caterers getCaterers() {

        return caterers;
    }

    public void present(CaterersResponse caterersResponse) {
        Caterers.CaterersBuilder caterersBuilder = Caterers.builder();
        caterersResponse.getCaterers()
                .stream()
                .map(BaseCatererPresenter::mapToCaterer)
                .forEach(caterersBuilder::addCaterer);
        caterers = caterersBuilder.build();
    }
}
