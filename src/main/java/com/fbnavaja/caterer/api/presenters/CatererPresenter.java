package com.fbnavaja.caterer.api.presenters;

import com.fbnavaja.caterer.api.ports.presenters.Caterer;
import com.fbnavaja.caterer.api.ports.presenters.CatererOutput;
import com.fbnavaja.caterer.api.ports.usecases.CatererResponse;

public class CatererPresenter  extends  BaseCatererPresenter implements CatererOutput {
    private Caterer caterer;
    public Caterer getCaterer() {
        return caterer;
    }
    public void present(CatererResponse catererResponse) {
        caterer = mapToCaterer(catererResponse);
    }
}
