package com.fbnavaja.caterer.api.presenters;

import com.fbnavaja.caterer.api.ports.presenters.CatererEvents;
import com.fbnavaja.caterer.api.ports.presenters.CatererEventsOutput;
import com.fbnavaja.caterer.api.ports.usecases.get.catererEvents.CatererEventsResponse;

public class CatererEventsPresenter extends  BaseCatererEventPresenter implements CatererEventsOutput {
    private CatererEvents catererEvents;

    public CatererEvents getCatererEvents() {

        return catererEvents;
    }

    public void present(CatererEventsResponse catererEventsResponse) {
        CatererEvents.CatererEventsBuilder catererEventsBuilder = CatererEvents.builder();
        catererEventsResponse.getCatererEvents()
                .stream()
                .map(BaseCatererEventPresenter::mapToCatererEvent)
                .forEach(catererEventsBuilder::addCatererEvent);
        catererEvents = catererEventsBuilder.build();
    }
}
