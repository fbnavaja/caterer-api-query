package com.fbnavaja.caterer.api.presenters;

import com.fbnavaja.caterer.api.ports.presenters.CatererEvent;
import com.fbnavaja.caterer.api.ports.presenters.CatererEventOutput;
import com.fbnavaja.caterer.api.ports.usecases.CatererEventResponse;

public class CatererEventPresenter extends  BaseCatererEventPresenter implements CatererEventOutput {
    private CatererEvent catererEvent;
    public CatererEvent getCatererEvent() {
        return catererEvent;
    }
    public void present(CatererEventResponse catererEventResponse) {
        catererEvent = mapToCatererEvent(catererEventResponse);
    }
}
