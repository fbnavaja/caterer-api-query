package com.fbnavaja.caterer.api.application.handlers;

import org.aspectj.lang.annotation.Aspect;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.stereotype.Component;


@Component
@Aspect
@EnableAspectJAutoProxy
public class LogHandler {
    private final Logger LOGGER = LoggerFactory.getLogger(this.getClass().getName());

}
