package com.fbnavaja.caterer.api.application.queries.get.caterereventbyid;

import com.fbnavaja.caterer.api.ports.presenters.CatererEvent;
import com.fbnavaja.caterer.api.ports.presenters.CatererEventOutput;
import com.fbnavaja.caterer.api.ports.usecases.get.caterereventsbyid.GetCatererEventByIdInput;
import com.fbnavaja.caterer.api.ports.usecases.get.caterereventsbyid.GetCatererEventByIdRequest;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/catererEvents")
public class GetCatererEventByIdCommand {
    private final GetCatererEventByIdInput useCase;
    private final CatererEventOutput presenter;

    public GetCatererEventByIdCommand(GetCatererEventByIdInput useCase, CatererEventOutput presenter) {
        this.useCase = useCase;
        this.presenter = presenter;
    }

    @GetMapping("/{id}")
    @ApiOperation(value = "Get Caterers", response = CatererEvent.class)
    public ResponseEntity exec(@PathVariable String id) {
        useCase.exec(new GetCatererEventByIdRequest(id));

        return ResponseEntity
                .ok()
                .body(presenter.getCatererEvent());
    }
}
