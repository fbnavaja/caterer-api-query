package com.fbnavaja.caterer.api.application.config;

import com.fbnavaja.caterer.api.database.jpa.JpaCatererDatabase;
import com.fbnavaja.caterer.api.database.jpa.JpaCatererEventDatabase;
import com.fbnavaja.caterer.api.database.jpa.repositories.CatererEventRepository;
import com.fbnavaja.caterer.api.database.jpa.repositories.CatererRepository;
import com.fbnavaja.caterer.api.ports.database.CatererDatabase;
import com.fbnavaja.caterer.api.ports.database.CatererEventDatabase;
import com.fbnavaja.caterer.api.ports.presenters.CatererEventOutput;
import com.fbnavaja.caterer.api.ports.presenters.CatererEventsOutput;
import com.fbnavaja.caterer.api.ports.presenters.CatererOutput;
import com.fbnavaja.caterer.api.ports.presenters.CaterersOutput;
import com.fbnavaja.caterer.api.ports.usecases.get.catererEvents.GetCatererEventsInput;
import com.fbnavaja.caterer.api.ports.usecases.get.caterereventsbyid.GetCatererEventByIdInput;
import com.fbnavaja.caterer.api.ports.usecases.get.caterers.GetCaterersInput;
import com.fbnavaja.caterer.api.ports.usecases.get.caterersbyid.GetCatererByIdInput;
import com.fbnavaja.caterer.api.presenters.CatererEventPresenter;
import com.fbnavaja.caterer.api.presenters.CatererEventsPresenter;
import com.fbnavaja.caterer.api.presenters.CatererPresenter;
import com.fbnavaja.caterer.api.presenters.CaterersPresenter;
import com.fbnavaja.caterer.api.usecases.get.catererEvents.GetCatererEvents;
import com.fbnavaja.caterer.api.usecases.get.catererbyid.GetCatererById;
import com.fbnavaja.caterer.api.usecases.get.caterereventbyid.GetCatererEventById;
import com.fbnavaja.caterer.api.usecases.get.caterers.GetCaterers;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;
import org.springframework.data.rest.webmvc.config.RepositoryRestMvcConfiguration;

@Configuration
@EntityScan("com.fbnavaja.caterer.api.database.jpa.data")
@EnableMongoRepositories("com.fbnavaja.caterer.api.database.jpa.repositories")
@Import(RepositoryRestMvcConfiguration.class)
public class CatererAppConfiguration {

    @Bean
    public CatererDatabase catererDatabase(CatererRepository catererRepository) {
        return new JpaCatererDatabase(catererRepository);
    }

    @Bean
    public CatererEventDatabase catererEventDatabase(CatererEventRepository catererEventRepository) {
        return new JpaCatererEventDatabase(catererEventRepository);
    }

    @Bean
    public GetCaterersInput getCaterersInput(CaterersOutput caterersOutput, CatererDatabase database) {
        return new GetCaterers(caterersOutput, database.catererGateway());
    }

    @Bean
    public CaterersOutput caterersOutput() {
        return new CaterersPresenter();
    }

    @Bean
    public GetCatererByIdInput getCatererByIdInput(CatererOutput catererOutput, CatererDatabase database) {
        return new GetCatererById(catererOutput, database.catererGateway());
    }

    @Bean
    public CatererOutput catererOutput() {

        return new CatererPresenter();
    }


    @Bean
    public CatererEventsOutput catererEventsOutput() {
        return new CatererEventsPresenter();
    }

    @Bean
    public GetCatererEventByIdInput getCatererEventByIdInput(CatererEventOutput catererEventOutput, CatererEventDatabase database) {
        return new GetCatererEventById(catererEventOutput, database.catererEventGateway());
    }

    @Bean
    public CatererEventOutput catererEventOutput() {

        return new CatererEventPresenter();
    }
    @Bean
    public GetCatererEventsInput getCatererEventsInput(CatererEventsOutput catererEventsOutput, CatererEventDatabase database) {
        return new GetCatererEvents(catererEventsOutput, database.catererEventGateway());
    }

}
