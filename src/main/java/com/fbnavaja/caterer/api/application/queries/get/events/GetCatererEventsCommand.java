package com.fbnavaja.caterer.api.application.queries.get.events;

import com.fbnavaja.caterer.api.ports.presenters.CatererEventsOutput;
import com.fbnavaja.caterer.api.ports.presenters.Caterers;
import com.fbnavaja.caterer.api.ports.usecases.get.catererEvents.GetCatererEventsInput;
import com.fbnavaja.caterer.api.ports.usecases.get.catererEvents.GetCatererEventsRequest;
import io.swagger.annotations.ApiOperation;
import org.springframework.data.rest.webmvc.RepositoryRestController;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@RepositoryRestController
@RequestMapping("/api/v1/catererEvents")
public class GetCatererEventsCommand {
    private final GetCatererEventsInput useCase;
    private final CatererEventsOutput presenter;

    public GetCatererEventsCommand(GetCatererEventsInput useCase, CatererEventsOutput presenter) {
        this.useCase = useCase;
        this.presenter = presenter;
    }


    @GetMapping
    @ApiOperation(value = "Get Caterer Events", response = Caterers.class)
    public ResponseEntity exec() {
        useCase.exec(new GetCatererEventsRequest());

        return ResponseEntity
                .ok()
                .body(presenter.getCatererEvents());
    }
}
