package com.fbnavaja.caterer.api.application.handlers;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class ExecutionTimeHandler {
    private final Logger LOGGER = LoggerFactory.getLogger(this.getClass().getName());
    private final String LOG_PREFIX = "---------------";

    @Around("@annotation(com.fbnavaja.caterer.api.application.handlers.MonitorExecutionTime)")
    public Object monitorExecutionTime(ProceedingJoinPoint joinPoint) throws  Throwable {
        String method = joinPoint.getSignature().getName();
        long startTime = System.nanoTime();
        Object proceed = joinPoint.proceed();
        long duration = (System.nanoTime() - startTime)/1_000;
        LOGGER.info("{} API {} execution time took {} microseconds", LOG_PREFIX, method, duration);

        return proceed;
    }
}
