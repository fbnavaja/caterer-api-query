package com.fbnavaja.caterer.api.application.queries.get.catererbyid;

import com.fbnavaja.caterer.api.ports.presenters.Caterer;
import com.fbnavaja.caterer.api.ports.presenters.CatererOutput;
import com.fbnavaja.caterer.api.ports.usecases.get.caterersbyid.GetCatererByIdInput;
import com.fbnavaja.caterer.api.ports.usecases.get.caterersbyid.GetCatererByIdRequest;
import io.swagger.annotations.ApiOperation;
import org.springframework.data.rest.webmvc.RepositoryRestController;
import org.springframework.hateoas.EntityModel;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RepositoryRestController
public class GetCatererByIdCommand {
    private final GetCatererByIdInput useCase;
    private final CatererOutput presenter;

    public GetCatererByIdCommand(GetCatererByIdInput useCase, CatererOutput presenter) {
        this.useCase = useCase;
        this.presenter = presenter;
    }


    @GetMapping(path="/caterers/{id}", produces = { APPLICATION_JSON_VALUE })
    @ApiOperation(value = "Get Caterers", response = Caterer.class)
    public ResponseEntity exec(@PathVariable String id) {
        useCase.exec(new GetCatererByIdRequest(id));

        return ResponseEntity.ok(EntityModel.of(
                presenter.getCaterer(),
                linkTo(methodOn(GetCatererByIdCommand.class).exec(id)).withSelfRel()));
    }
}
