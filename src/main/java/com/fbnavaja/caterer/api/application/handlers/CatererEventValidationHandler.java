package com.fbnavaja.caterer.api.application.handlers;

import com.fbnavaja.caterer.api.domain.entities.Caterer;
import org.springframework.data.rest.core.annotation.HandleBeforeCreate;
import org.springframework.data.rest.core.annotation.HandleBeforeDelete;
import org.springframework.data.rest.core.annotation.HandleBeforeSave;
import org.springframework.data.rest.core.annotation.RepositoryEventHandler;

@RepositoryEventHandler
public class CatererEventValidationHandler {

    @HandleBeforeCreate
    public void validateCatererCreateEvent(Caterer caterer) {

    }

    @HandleBeforeSave
    public void validateCatererSaveEvent(Caterer caterer) {

    }

    @HandleBeforeDelete
    public void validateCatererDeleteEvent(Caterer caterer) {

    }
}
