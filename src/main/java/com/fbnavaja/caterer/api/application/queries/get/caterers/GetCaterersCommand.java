package com.fbnavaja.caterer.api.application.queries.get.caterers;

import com.fbnavaja.caterer.api.ports.presenters.Caterer;
import com.fbnavaja.caterer.api.ports.presenters.Caterers;
import com.fbnavaja.caterer.api.ports.presenters.CaterersOutput;
import com.fbnavaja.caterer.api.ports.usecases.get.caterers.GetCaterersInput;
import com.fbnavaja.caterer.api.ports.usecases.get.caterers.GetCaterersRequest;
import io.swagger.annotations.ApiOperation;
import org.springframework.data.rest.webmvc.RepositoryRestController;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.Link;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RepositoryRestController
public class GetCaterersCommand {
    private final GetCaterersInput useCase;
    private final CaterersOutput presenter;

    public GetCaterersCommand(GetCaterersInput useCase, CaterersOutput presenter) {
        this.useCase = useCase;
        this.presenter = presenter;
    }

    @GetMapping(path="/caterers", produces = { APPLICATION_JSON_VALUE })
    @ApiOperation(value = "Get Caterers", response = Caterers.class)
    public ResponseEntity exec() {
        useCase.exec(new GetCaterersRequest());

        final ArrayList<Link> links = new ArrayList<>();
        links.add(linkTo(GetCaterersCommand.class).withSelfRel());
        List<Caterer> caterers = presenter.getCaterers().getCaterers();

        List<EntityModel<Caterer>> itemResources =  caterers.stream()
                .map(item -> EntityModel.of(item,
                        linkTo(GetCaterersCommand.class).slash(item.getId()).withSelfRel()))
                .collect(Collectors.toList());

        return ResponseEntity.ok(CollectionModel.of(
                itemResources,
                linkTo(methodOn(GetCaterersCommand.class).exec()).withSelfRel()));
    }
}
