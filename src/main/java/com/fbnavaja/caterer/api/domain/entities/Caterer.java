package com.fbnavaja.caterer.api.domain.entities;

import lombok.*;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.util.UUID;

@NoArgsConstructor
@AllArgsConstructor
@Builder
@Data
@ToString
@EqualsAndHashCode
public class Caterer {
    private String id;
    private String name;
    private Location location;
    private Capacity capacity;
    private ContactInfo contactInfo;

    @Builder
    @Getter
    public static class Location {
        private String city;
        private String street;
        private int zip;
    }

    @Builder
    @Getter
    public static class Capacity {
        private int min_guests;
        private int max_guests;
    }

    @Builder
    @Getter
    public static class ContactInfo {
        private String phoneNumber;
        private String mobileNumber;
        private String emailAddress;
    }

}
