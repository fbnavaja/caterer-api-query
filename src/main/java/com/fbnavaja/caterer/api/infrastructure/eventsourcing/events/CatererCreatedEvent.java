package com.fbnavaja.caterer.api.infrastructure.eventsourcing.events;

import lombok.Builder;
import lombok.Data;

import java.time.LocalDateTime;
import java.util.UUID;

@Data
@Builder
public class CatererCreatedEvent {
    String catererId;
    String catererName;
    LocalDateTime createdDateTime;
}
