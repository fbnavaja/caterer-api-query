package com.fbnavaja.caterer.api.usecases.get;

import com.fbnavaja.caterer.api.domain.entities.Caterer;
import com.fbnavaja.caterer.api.ports.usecases.CatererResponse;

import java.util.UUID;

public class GetCatererBase {
    protected GetCatererBase() {}

    public static CatererResponse buildCatererResponse(Caterer caterer) {
        return new CatererResponse(
                caterer.getId(),
                caterer.getName(),
                caterer.getLocation().getCity(),
                caterer.getLocation().getStreet(),
                caterer.getLocation().getZip(),
                caterer.getCapacity().getMin_guests(),
                caterer.getCapacity().getMax_guests(),
                caterer.getContactInfo().getPhoneNumber(),
                caterer.getContactInfo().getMobileNumber(),
                caterer.getContactInfo().getEmailAddress()
        );
    }
}
