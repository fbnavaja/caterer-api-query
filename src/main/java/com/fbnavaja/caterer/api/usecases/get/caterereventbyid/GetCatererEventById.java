package com.fbnavaja.caterer.api.usecases.get.caterereventbyid;

import com.fbnavaja.caterer.api.domain.entities.CatererEvent;
import com.fbnavaja.caterer.api.ports.database.CatererEventGateway;
import com.fbnavaja.caterer.api.ports.presenters.CatererEventOutput;
import com.fbnavaja.caterer.api.ports.usecases.get.caterereventsbyid.GetCatererEventByIdInput;
import com.fbnavaja.caterer.api.ports.usecases.get.caterereventsbyid.GetCatererEventByIdRequest;
import com.fbnavaja.caterer.api.usecases.get.GetCatererBase;

import static com.fbnavaja.caterer.api.usecases.get.GetCatererEventBase.buildCatererEventResponse;

public class GetCatererEventById extends GetCatererBase implements GetCatererEventByIdInput {
    private final CatererEventOutput presenter;
    private final CatererEventGateway catererEventGateway;

    public GetCatererEventById(CatererEventOutput presenter, CatererEventGateway catererEventGateway) {
        this.presenter = presenter;
        this.catererEventGateway = catererEventGateway;
    }

    @Override
    public void exec(GetCatererEventByIdRequest request) {
        CatererEvent catererEvent = catererEventGateway.getCatererEventById(request.getId());

        presenter.present(buildCatererEventResponse(catererEvent));
    }
}
