package com.fbnavaja.caterer.api.usecases.get.caterers;

import com.fbnavaja.caterer.api.ports.database.CatererGateway;
import com.fbnavaja.caterer.api.ports.presenters.CaterersOutput;
import com.fbnavaja.caterer.api.ports.usecases.get.caterers.CaterersResponse;
import com.fbnavaja.caterer.api.ports.usecases.get.caterers.GetCaterersInput;
import com.fbnavaja.caterer.api.ports.usecases.get.caterers.GetCaterersRequest;
import com.fbnavaja.caterer.api.usecases.get.GetCatererBase;

public class GetCaterers extends GetCatererBase implements GetCaterersInput {
    private final CaterersOutput presenter;
    private final CatererGateway catererGateway;

    public GetCaterers(CaterersOutput presenter, CatererGateway catererGateway) {
        this.presenter = presenter;
        this.catererGateway = catererGateway;
    }

    @Override
    public void exec(GetCaterersRequest request) {
        CaterersResponse.CaterersResponseBuilder results = CaterersResponse.builder();
        catererGateway.getAllCaterers()
                .forEach(caterer -> results.addCaterer(buildCatererResponse(caterer)));

        presenter.present(results.build());
    }
}
