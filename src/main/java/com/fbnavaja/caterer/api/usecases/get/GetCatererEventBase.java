package com.fbnavaja.caterer.api.usecases.get;

import com.fbnavaja.caterer.api.domain.entities.CatererEvent;
import com.fbnavaja.caterer.api.ports.usecases.CatererEventResponse;

public class GetCatererEventBase {
    protected GetCatererEventBase() {}

    public static CatererEventResponse buildCatererEventResponse(CatererEvent catererEvent) {
        return new CatererEventResponse(
                catererEvent.getId(),
                catererEvent.getCatererId(),
                catererEvent.getCatererName(),
                catererEvent.getEventDescription(),
                catererEvent.getCreatedDateTime()
        );
    }
}
