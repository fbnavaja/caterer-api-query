package com.fbnavaja.caterer.api.usecases.get.catererbyid;

import com.fbnavaja.caterer.api.domain.entities.Caterer;
import com.fbnavaja.caterer.api.ports.database.CatererGateway;
import com.fbnavaja.caterer.api.ports.presenters.CatererOutput;
import com.fbnavaja.caterer.api.ports.usecases.get.caterersbyid.GetCatererByIdInput;
import com.fbnavaja.caterer.api.ports.usecases.get.caterersbyid.GetCatererByIdRequest;
import com.fbnavaja.caterer.api.usecases.get.GetCatererBase;

public class GetCatererById extends GetCatererBase implements GetCatererByIdInput {
    private final CatererOutput presenter;
    private final CatererGateway catererGateway;

    public GetCatererById(CatererOutput presenter, CatererGateway catererGateway) {
        this.presenter = presenter;
        this.catererGateway = catererGateway;
    }

    @Override
    public void exec(GetCatererByIdRequest request) {
        Caterer caterer = catererGateway.getCatererById(request.getId());

        presenter.present(buildCatererResponse(caterer));
    }
}
