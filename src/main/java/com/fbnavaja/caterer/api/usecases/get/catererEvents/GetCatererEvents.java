package com.fbnavaja.caterer.api.usecases.get.catererEvents;

import com.fbnavaja.caterer.api.ports.database.CatererEventGateway;
import com.fbnavaja.caterer.api.ports.presenters.CatererEventsOutput;
import com.fbnavaja.caterer.api.ports.usecases.get.catererEvents.CatererEventsResponse;
import com.fbnavaja.caterer.api.ports.usecases.get.catererEvents.GetCatererEventsInput;
import com.fbnavaja.caterer.api.ports.usecases.get.catererEvents.GetCatererEventsRequest;
import com.fbnavaja.caterer.api.usecases.get.GetCatererEventBase;

public class GetCatererEvents extends GetCatererEventBase implements GetCatererEventsInput {
    private final CatererEventsOutput presenter;
    private final CatererEventGateway catererEventGateway;

    public GetCatererEvents(CatererEventsOutput presenter, CatererEventGateway catererEventGateway) {
        this.presenter = presenter;
        this.catererEventGateway = catererEventGateway;
    }


    @Override
    public void exec(GetCatererEventsRequest request) {
        CatererEventsResponse.CatererEventsResponseBuilder results = CatererEventsResponse.builder();
        catererEventGateway.getAllEvents()
                .forEach(catererEvent -> results.addCatererEvent(buildCatererEventResponse(catererEvent)));

        presenter.present(results.build());
    }

}
