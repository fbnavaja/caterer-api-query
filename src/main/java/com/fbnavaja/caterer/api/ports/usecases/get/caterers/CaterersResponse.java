package com.fbnavaja.caterer.api.ports.usecases.get.caterers;

import com.fbnavaja.caterer.api.ports.usecases.CatererResponse;
import lombok.*;

import java.util.List;

@Getter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class CaterersResponse {
    @Singular("addCaterer") private List<CatererResponse> caterers;
}
