package com.fbnavaja.caterer.api.ports.usecases.get.caterereventsbyid;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class GetCatererEventByIdRequest {
    private String id;
}
