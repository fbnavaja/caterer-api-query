package com.fbnavaja.caterer.api.ports.database;

import com.fbnavaja.caterer.api.domain.entities.Caterer;

import java.util.Collection;
import java.util.UUID;

public interface CatererGateway {
    Collection<Caterer> getAllCaterers();
    Caterer getCatererById(String catererId);
    String createCaterer(Caterer caterer);
    void saveCaterer(Caterer caterer);
    void deleteCaterer(String catererId);
    Collection<Caterer> getCatererByName(String catererName);
}
