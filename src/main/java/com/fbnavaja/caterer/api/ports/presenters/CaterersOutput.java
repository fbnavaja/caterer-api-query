package com.fbnavaja.caterer.api.ports.presenters;

import com.fbnavaja.caterer.api.ports.usecases.CatererResponse;
import com.fbnavaja.caterer.api.ports.usecases.get.caterers.CaterersResponse;

public interface CaterersOutput {
    Caterers getCaterers();
    void present(CaterersResponse caterersResponse);
}
