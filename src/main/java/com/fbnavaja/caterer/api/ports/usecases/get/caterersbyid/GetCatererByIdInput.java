package com.fbnavaja.caterer.api.ports.usecases.get.caterersbyid;

public interface GetCatererByIdInput {
    void exec(GetCatererByIdRequest request);
}
