package com.fbnavaja.caterer.api.ports.usecases.get.catererEvents;

public interface GetCatererEventsInput {
    void exec(GetCatererEventsRequest request);
}
