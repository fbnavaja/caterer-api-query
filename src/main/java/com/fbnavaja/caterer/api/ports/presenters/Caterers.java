package com.fbnavaja.caterer.api.ports.presenters;

import lombok.*;

import java.util.List;
import java.util.UUID;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Getter
public class Caterers {
    @Singular("addCaterer") private List<Caterer> caterers;
}
