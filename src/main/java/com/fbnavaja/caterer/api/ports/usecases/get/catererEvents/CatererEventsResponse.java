package com.fbnavaja.caterer.api.ports.usecases.get.catererEvents;

import com.fbnavaja.caterer.api.ports.usecases.CatererEventResponse;
import lombok.*;

import java.util.List;

@Getter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class CatererEventsResponse {
    @Singular("addCatererEvent") private List<CatererEventResponse> catererEvents;
}
