package com.fbnavaja.caterer.api.ports.presenters;

import com.fbnavaja.caterer.api.ports.usecases.get.catererEvents.CatererEventsResponse;

public interface CatererEventsOutput {
    CatererEvents getCatererEvents();
    void present(CatererEventsResponse catererEventsResponse);
}
