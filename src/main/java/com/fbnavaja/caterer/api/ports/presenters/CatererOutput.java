package com.fbnavaja.caterer.api.ports.presenters;

import com.fbnavaja.caterer.api.ports.usecases.CatererResponse;

public interface CatererOutput {
    Caterer getCaterer();
    void present(CatererResponse catererResponse);
}
