package com.fbnavaja.caterer.api.ports.usecases.get.caterersbyid;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.util.UUID;

@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class GetCatererByIdRequest {
    private String id;
}
