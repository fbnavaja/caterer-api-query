package com.fbnavaja.caterer.api.ports.usecases.get.caterers;

public interface GetCaterersInput {
    void exec(GetCaterersRequest request);
}
