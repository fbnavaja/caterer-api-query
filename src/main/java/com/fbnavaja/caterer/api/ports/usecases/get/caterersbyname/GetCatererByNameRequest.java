package com.fbnavaja.caterer.api.ports.usecases.get.caterersbyname;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.util.UUID;

@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class GetCatererByNameRequest {
    private String name;
}
