package com.fbnavaja.caterer.api.ports.presenters;

import lombok.Builder;
import lombok.Getter;

import java.time.LocalDateTime;

@Builder
@Getter
public class CatererEvent {

    private String id;
    private String catererId;
    private String catererName;
    private String eventDescription;
    private LocalDateTime createdDateTime;

}
