package com.fbnavaja.caterer.api.ports.usecases.get.caterereventsbyid;

public interface GetCatererEventByIdInput {
    void exec(GetCatererEventByIdRequest request);
}
