package com.fbnavaja.caterer.api.ports.presenters;

import lombok.*;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Getter
public class CatererEvents {
    @Singular("addCatererEvent") private List<CatererEvent> catererEvents;
}
