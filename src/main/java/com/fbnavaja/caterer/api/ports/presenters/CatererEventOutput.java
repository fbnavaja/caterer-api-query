package com.fbnavaja.caterer.api.ports.presenters;

import com.fbnavaja.caterer.api.ports.usecases.CatererEventResponse;

public interface CatererEventOutput {
    CatererEvent getCatererEvent();
    void present(CatererEventResponse catererEventResponse);
}
