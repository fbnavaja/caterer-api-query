package com.fbnavaja.caterer.api.ports.database;

import com.fbnavaja.caterer.api.domain.entities.Caterer;
import com.fbnavaja.caterer.api.domain.entities.CatererEvent;

import java.util.Collection;

public interface CatererEventGateway {
    Collection<CatererEvent> getAllEvents();
    CatererEvent getCatererEventById(String eventId);
    void createCatererEvent(CatererEvent catererEvent);
}
