package com.fbnavaja.caterer.api.ports.database;

public interface CatererEventDatabase {
    CatererEventGateway catererEventGateway();
}
