package com.fbnavaja.caterer.api.ports.usecases.get.caterersbyname;

public interface GetCatererByNameInput {
    void exec(GetCatererByNameRequest request);
}
